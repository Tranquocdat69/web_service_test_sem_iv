/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.Gson;
import dao.CarDaoImpl;
import dao.ICarDao;
import entities.TblCar;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author dat-tq
 */
@Path("/car")
public class CarApiController {
    private ICarDao _dao;

    public CarApiController() {
        this._dao = new CarDaoImpl();
    }
    
    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAll(){
        Gson gs = new Gson();
        String json = gs.toJson(_dao.getAll());
        return json;
    }
    
    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public String insert(String json){
        Gson gs = new Gson();
        TblCar car = gs.fromJson(json, TblCar.class);
        String message = _dao.insert(car);
        return message;
    }
}
