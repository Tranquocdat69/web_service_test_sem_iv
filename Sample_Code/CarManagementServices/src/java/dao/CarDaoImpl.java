/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.TblCar;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

/**
 *
 * @author dat-tq
 */
public class CarDaoImpl implements ICarDao {

    private SessionFactory _sessionFactory;

    public CarDaoImpl() {
        this._sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public List<TblCar> getAll() {
        Session s = _sessionFactory.openSession();
        s.beginTransaction();
        List<TblCar> cars = s.createQuery("from TblCar").list();
        s.getTransaction().commit();
        s.close();
        return cars;
    }

    @Override
    public String insert(TblCar car) {
        String message = "";
        Session s = _sessionFactory.openSession();
        try {
            s.beginTransaction();
            s.save(car);
            s.getTransaction().commit();
        } catch (Exception e) {
            s.getTransaction().rollback();
            message = e.getMessage();
        }finally{
            s.close();
        }
        return message;
    }

}
