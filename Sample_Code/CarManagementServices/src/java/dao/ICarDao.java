/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.TblCar;
import java.util.List;

/**
 *
 * @author dat-tq
 */
public interface ICarDao {
    List<TblCar> getAll();
    String insert(TblCar car);
}
