/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dat-tq
 */
@Entity
@Table(name = "TblCar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblCar.findAll", query = "SELECT t FROM TblCar t")
    , @NamedQuery(name = "TblCar.findByCarId", query = "SELECT t FROM TblCar t WHERE t.carId = :carId")
    , @NamedQuery(name = "TblCar.findByCarName", query = "SELECT t FROM TblCar t WHERE t.carName = :carName")
    , @NamedQuery(name = "TblCar.findByProducer", query = "SELECT t FROM TblCar t WHERE t.producer = :producer")
    , @NamedQuery(name = "TblCar.findByYearMaking", query = "SELECT t FROM TblCar t WHERE t.yearMaking = :yearMaking")
    , @NamedQuery(name = "TblCar.findByPrice", query = "SELECT t FROM TblCar t WHERE t.price = :price")})
public class TblCar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CarId")
    private Integer carId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "CarName")
    private String carName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "Producer")
    private String producer;
    @Column(name = "YearMaking")
    private Integer yearMaking;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Price")
    private Double price;

    public TblCar() {
    }

    public TblCar(Integer carId) {
        this.carId = carId;
    }

    public TblCar(Integer carId, String carName, String producer) {
        this.carId = carId;
        this.carName = carName;
        this.producer = producer;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public Integer getYearMaking() {
        return yearMaking;
    }

    public void setYearMaking(Integer yearMaking) {
        this.yearMaking = yearMaking;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carId != null ? carId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblCar)) {
            return false;
        }
        TblCar other = (TblCar) object;
        if ((this.carId == null && other.carId != null) || (this.carId != null && !this.carId.equals(other.carId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.TblCar[ carId=" + carId + " ]";
    }
    
}
