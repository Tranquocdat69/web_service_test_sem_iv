
import controllers.CarApiController;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dat-tq
 */
@ApplicationPath("/api")
public class Startup extends Application{
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> services = new HashSet<>();
        ConfigureServices(services);
        return services; 
    }

    private void ConfigureServices(Set<Class<?>> services) {
        services.add(CarApiController.class);
    }
}
