﻿create database WEBSERVICE_Exam_07
go
use WEBSERVICE_Exam_07
go
create table TblCar(
	CarId int primary key identity, 
	CarName nvarchar(80) not null, 
	Producer nvarchar(120) not null, 
	YearMaking int default 2000, 
	Price float default 50000000
)
go
insert into TblCar (CarName,Producer,Price,YearMaking) values
('Santafe 2021','Huyndai',9999,2021),
('Mazda CX-5','Mazda',2000000,2022),
('Mercedes 2016','Mercedes',1000000,2016)