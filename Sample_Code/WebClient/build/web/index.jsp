<%-- 
    Document   : index
    Created on : Oct 12, 2021, 10:18:44 PM
    Author     : dat-tq
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cars</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row">
            <div class="box">
                <center>
                    <a href="CreateCartServlet?action=display">Thêm mới xe</a>
                </center>
                <br>
                <div class="box-body">
                    <table class="table" border="1px">
                        <thead>
                            <tr>
                                <th>Mã</th>
                                <th>Tên xe</th>
                                <th>Nhà sản xuất</th>
                                <th>Năm sản xuất</th>
                                <th>Giá</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${cars}">
                                <tr>
                                    <td>${item.carId}</td>
                                    <td>${item.carName}</td>
                                    <td>${item.producer}</td>
                                    <td>${item.yearMaking}</td>
                                    <td>
                                        <fmt:formatNumber value="${item.price}" type="currency"/>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <c:if test="${message != null}">
            <script>
                alert('${message}');
            </script>
        </c:if>
    </body>
</html>
