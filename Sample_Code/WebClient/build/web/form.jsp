<%-- 
    Document   : form
    Created on : Oct 12, 2021, 10:46:52 PM
    Author     : dat-tq
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add new car</title>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="box">
            <div class="box-header">
                <h1 style="text-align: center;">Thêm mới xe hơi</h1>

            </div>
            <div class="box-body">
                <form action="CreateCartServlet?action=create" method="POST">
                    <table style="margin: auto;padding: 0;text-align: left;">
                        <tr>
                            <th>Tên xe:</th>
                            <td>
                                <input type="text" name="name" value="${model.carName}">
                            </td>
                        </tr>
                        <tr>
                            <th>Nhà sản xuất:</th>
                            <td>
                                <input type="text" name="producer" value="${model.producer}">
                            </td>
                        </tr>
                        <tr>
                            <th>Năm sản xuất:</th>
                            <td>
                                <input type="number" name="year" value="${model.yearMaking}">
                            </td>
                        </tr>
                        <tr>
                            <th>Giá:</th>
                            <td>
                                <input type="number" name="price" value="${model.price}">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <button type="submit">Thêm mới</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <c:if test="${message != null}">
            <script>
                var el = document.createElement('h3');
                el.setAttribute("style", "text-align: center;color: red;");
                el.innerHTML = "create fail!!!!";
                document.querySelector('.box-header').appendChild(el);
                console.error(`${message}`);
            </script>
        </c:if>
    </body>
</html>
