/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Car;
import repository.CarRepository;

/**
 *
 * @author dat-tq
 */
public class CreateCartServlet extends HttpServlet {

    private final CarRepository _repository = new CarRepository();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String action = (request.getParameter("action") != null) ? request.getParameter("action") : "";
        switch (action) {
            case "display":
                request.setAttribute("model", new Car());
                request.getRequestDispatcher("/form.jsp").forward(request, response);
                break;
            case "create":
                String name = request.getParameter("name");
                String producer = request.getParameter("producer");
                String year = request.getParameter("year").isEmpty()? "0":request.getParameter("year");
                String price = request.getParameter("price").isEmpty() ? "0":request.getParameter("price");
                
                Car car = new Car();
                car.setCarName(name);
                car.setProducer(producer);
                car.setYearMaking(Integer.parseInt(year));
                car.setPrice(Double.parseDouble(price));
                
                String message = _repository.createCar(car);
                if (message.isEmpty()) {
                    response.sendRedirect(request.getContextPath());
                }else {
                    request.setAttribute("model", car);
                    request.setAttribute("message", message);
                    request.getRequestDispatcher("/form.jsp").forward(request, response);
                }
                break;
            default:
                response.sendRedirect("CarsServlet");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
