/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import com.google.gson.Gson;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;


public class RestServicesHelper {
    private final Client _client;
    
    public RestServicesHelper() {
        _client = ClientBuilder.newClient();
    }
    
    private Builder acceptJson(String target){
        WebTarget resource = _client.target(target);
        Builder builder = resource.request();
        builder.accept(MediaType.APPLICATION_JSON);
        return builder;
    }
    
    public String httpGet(String url){
        Builder request = acceptJson(url);
        return request.get(String.class);
    }
    
    public <T> T httpPost(String url, Object obj, Class<T> type){
        Builder request = acceptJson(url);
        Gson gs = new Gson();
        String json = gs.toJson(obj);
        return request.post(Entity.entity(json, MediaType.APPLICATION_JSON), type);
    }
}
