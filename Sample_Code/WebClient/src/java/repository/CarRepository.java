/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import com.google.gson.Gson;
import helpers.RestServicesHelper;
import java.util.List;
import javax.ws.rs.core.GenericType;
import models.Car;

/**
 *
 * @author dat-tq
 */
public class CarRepository {

    private final RestServicesHelper _helper;

    public CarRepository() {
        _helper = new RestServicesHelper();
    }

    public List<Car> loadCars() {
        String json = _helper.httpGet("http://localhost:8080/CarManagementServices/api/car");
        Gson gs = new Gson();
        GenericType<List<Car>> type = new GenericType<List<Car>>() {
        };
        return gs.fromJson(json, type.getType());
    }
    
    public String createCar(Car car){
        return _helper.httpPost("http://localhost:8080/CarManagementServices/api/car/create", car, String.class);
    }
}
